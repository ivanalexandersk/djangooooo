from django.db import models

# Create your models here.

class Sitio(models.Model):
    identifie= models.IntegerField()
    nombre= models.TextField()
    descripcion= models.TextField()
    latitud= models.DecimalField(max_digits=10,decimal_places=7)
    longitud= models.DecimalField(max_digits=10,decimal_places=7)
    url= models.CharField(max_length=100)
